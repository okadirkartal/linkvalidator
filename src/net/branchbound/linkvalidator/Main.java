package net.branchbound.linkvalidator;

import httpclient.ws.WebsocketExample;

import java.io.IOException;
import java.net.http.HttpClient;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws IOException, Exception {
        // httpClientSyncSample();
        // httpClientAsyncSample();
        WebsocketExample.Sample();
    }

    static void httpClientSyncSample() throws IOException {
        LinkValidatorSync.client = HttpClient.newHttpClient();
        Files.lines(Path.of("urls.txt")).map(LinkValidatorSync::validateLink).forEach(System.out::println);
    }

    static void httpClientAsyncSample() throws IOException {
        LinkValidatorAsync.client = HttpClient.newBuilder()
                .connectTimeout(Duration.ofSeconds(3))
                .followRedirects(HttpClient.Redirect.NORMAL)
                .build();

        var futures = Files.lines(Path.of("urls.txt")).map(LinkValidatorAsync::validateLink)
                .collect(Collectors.toList());

        futures.stream().map(CompletableFuture::join)
                .forEach(System.out::println);

    }
}
