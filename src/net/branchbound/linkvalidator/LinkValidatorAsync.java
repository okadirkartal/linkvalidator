package net.branchbound.linkvalidator;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.util.concurrent.CompletableFuture;

public class LinkValidatorAsync {

    public static HttpClient client;

    public static CompletableFuture<String> validateLink(String link) {
        HttpRequest request = HttpRequest.newBuilder(URI.create(link))
                .timeout(Duration.ofSeconds(2))

                .GET().build();

        return client.sendAsync(request,
                HttpResponse.BodyHandlers.discarding())
                .thenApply(LinkValidatorAsync::responseToString)
                .exceptionally(e -> String.format("%s ->%s", link, false));

    }

    private static String responseToString(HttpResponse<Void> response) {
        int status = response.statusCode();
        boolean success = status >= 200 && status <= 299;
        return String.format("%s -> %s (status %s)", response.uri(), success, status);
    }
}
